<?php

	include "banco.php";
	include "util.php";
    date_default_timezone_set('America/Sao_Paulo');

	$descr = $_POST["descr"];
	$cdpara = $_POST["cdpara"];

	$Flag = true;

	if (empty($descr)==true ){
		$demens = "Descrição não pode ficar em branco!";
		$detitu = "Clínicas Estéticas OnLine&copy; | Cadastro de Tratamentos";
		header('Location: mensagem.php?demens='.$demens.'&detitu='.$detitu);
		$Flag=false;
	}

	$cdplan = str_pad($cdplan, 2, '0', STR_PAD_LEFT);

	$aTrab = ConsultarDados('','','','select * from especialidades where descr ='.$descr.' And cdpara ='.$cdpara);
	if ( count($aTrab) > 0) {
		$demens = "Código já cadastrado!";
		$detitu = "Clínicas Estéticas OnLine&copy; | Cadastro de Tratamentos";
		header('Location: mensagem.php?demens='.$demens.'&detitu='.$detitu);
		$Flag=false;
	}

	if ($Flag == true) {

		//campos da tabela
		$aNomes=array();
		$aNomes[]= "descr";
		$aNomes[]= "cdpara";
	
		//dados da tabela
		$aDados=array();
		$aDados[]= $descr;
		$aDados[]= $cdpara;

		IncluirDados("especialidades", $aDados, $aNomes);

		$demens = "Cadastro efetuado com sucesso!";
		$detitu = "Clínicas Estéticas OnLine&copy; | Cadastro de Tratamentos";
		$devolt = "especialidades.php";
		header('Location: mensagem.php?demens='.$demens.'&detitu='.$detitu.'&devolt='.$devolt);
	}

?>