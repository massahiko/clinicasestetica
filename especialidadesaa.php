<?php

	include "banco.php";
	include "util.php";
    date_default_timezone_set('America/Sao_Paulo');

	$id = $_POST["id"];
	$descr = $_POST["descr"];
	$cdpara = $_POST["cdpara"];

	$Flag = true;

	switch (get_post_action('edita','apaga')) {
    case 'edita':

		if ($Flag == true){

			$demens = "Atualização efetuada com sucesso!";

			//campos da tabela
			$aNomes=array();
			$aNomes[]= "id";
			$aNomes[]= "descr";
			$aNomes[]= "cdpara";
		
			//dados da tabela
			$aDados=array();
			$aDados[]= $id;
			$aDados[]= $descr;
			$aDados[]= $cdpara;

			AlterarDados("especialidades", $aDados, $aNomes,"id", $id);

		}

		break;
    case 'apaga':
		$demens = "Exclusão efetuada com sucesso!";

		ExcluirDados("especialidades", "cdplan", $cdplan);

		break;
    default:
		$demens = "Ocorreu um problema na atualização/exclusão. Se persistir contate o suporte!";
	}

	if ($Flag == true) {
		$detitu = "Clínicas Estéticas OnLine&copy; | Cadastro de Tratamentos";
		$devolt = "especialidades.php";
		header('Location: mensagem.php?demens='.$demens.'&detitu='.$detitu.'&devolt='.$devolt);
	}

?>