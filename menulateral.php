        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                                <img alt="foto" width="80" height="80" class="img-circle" src="<?php echo $defoto; ?>" />
                                 </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $deusua; ?></strong>
                                 </span> <span class="text-muted text-xs block"><?php echo $detipo; ?><b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="meusdados.php">Alterar Meus Dados</a></li>
                                <li><a href="minhasenha.php">Alterar Minha Senha</a></li>
                                <li class="divider"></li>
                                <li><a href="index.html">Sair</a></li>
                            </ul>
                        </div>Benvindo a Clínicas Estética OnLine©
                    </li>
                    <li class="active">
                        <a href="index.php"><i class="fa fa-home"></i><span class="nav-label">Menu Principal</span></a>
                    </li>
                    <?php if ($cdtipo == 'M' or $cdtipo == 'A'){?>
                        <li>
                            <a href="index.php"><i class="fa fa-edit"></i><span class="nav-label">Administração</span><span class="caret"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <?php if (count(Acesso("1.1",$cdnive))>0){?>
                                    <li><a href="usuarios.php">Usuários</a></li>
                                <?php }?>
                                
                                <?php if (count(Acesso("1.2",$cdnive))>0){?>
                                    <li><a href="parametros.php">Parâmetros</a></li>
                                <?php }?>
                               
                                <?php if (count(Acesso("1.5",$cdnive))>0 and $cdtipo == 'M'){?>
                                    <li><a href="log.php">Histórico de Ações</a></li>
                                <?php }?>
                            </ul>
                        </li>
                    <?php }?>

                    <?php if (count(Acesso("2.7",$cdnive))>0){?>
                        <li>
                            <?php if ($cdtipo == 'M' or $cdtipo == 'A' or $cdtipo == 'F' or $cdtipo == 'O'){?>
                                <a href="agenda.php"><i class="fa fa-calendar"></i><span class="nav-label">Agenda</span></a>
                            <?php }?>
                            <?php if ($cdtipo == 'C'){?>
                                <a href="agenda.php"><i class="fa fa-calendar"></i><span class="nav-label">Minhas Consultas</span></a>
                            <?php }?>    
                        </li>
                    <?php }?>

                    
                    <?php if ($cdtipo == 'M' or $cdtipo == 'A' or $cdtipo == 'F' or $cdtipo == 'O'){?>
                        <li>
                            <a href="colaboradores.php"><i class="fa fa-user-md"></i><span class="nav-label">Colaborador</span></a>
                        </li>
                    <?php }?>

                    <?php if ($cdtipo == 'M' or $cdtipo == 'A' or $cdtipo == 'F' or $cdtipo == 'O'){?>
                        <li>
                            <a href="clientes.php"><i class="fa fa-male"></i><span class="nav-label">Clientes</span></a>
                        </li>
                    <?php }?>

                    <?php if ($cdtipo == 'M'){?>
                        <li>
                            <a href="contas.php"><i class="fa fa-calculator"></i><span class="nav-label">Contas a Pagar/Receber</span></a>
                        </li>
                    <?php }?>

                    <?php if ($cdtipo == 'M'){?>
                        <li>
                            <a href="fluxo.php"><i class="fa fa-money"></i><span class="nav-label">Fluxo de Caixa</span></a>
                        </li>
                    <?php }?>
                    <li>
                        <a href="especialidades.php"><i class="glyphicon glyphicon-list-alt"></i><span class="nav-label">Tratamentos</span></a>
                    </li>
                </ul>
            </div>
        </nav>
