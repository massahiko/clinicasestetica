-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 10.1.2.122:3306
-- Tempo de geração: 13/05/2019 às 21:33
-- Versão do servidor: 10.2.16-MariaDB
-- Versão do PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `u372777043_clini`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `cdusua` varchar(14) NOT NULL,
  `deusua` varchar(100) DEFAULT NULL,
  `desenh` varchar(100) DEFAULT NULL,
  `demail` varchar(255) DEFAULT NULL,
  `defoto` varchar(255) DEFAULT NULL,
  `cdtipo` varchar(1) DEFAULT NULL,
  `nrtele` varchar(20) DEFAULT NULL,
  `nrcelu` varchar(20) DEFAULT NULL,
  `deobse` varchar(500) DEFAULT NULL,
  `deende` varchar(100) DEFAULT NULL,
  `nrende` int(11) DEFAULT NULL,
  `decomp` varchar(100) DEFAULT NULL,
  `debair` varchar(100) DEFAULT NULL,
  `decida` varchar(100) DEFAULT NULL,
  `cdesta` varchar(2) DEFAULT NULL,
  `nrcepi` varchar(8) DEFAULT NULL,
  `dtcada` date DEFAULT NULL,
  `flativ` char(1) DEFAULT NULL,
  `deespe` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `usuarios`
--

INSERT INTO `usuarios` (`cdusua`, `deusua`, `desenh`, `demail`, `defoto`, `cdtipo`, `nrtele`, `nrcelu`, `deobse`, `deende`, `nrende`, `decomp`, `debair`, `decida`, `cdesta`, `nrcepi`, `dtcada`, `flativ`, `deespe`) VALUES
('00000000000000', 'Recepcionista', 'c6f057b86584942e415435ffb1fa93d4', 'rec@rec.com', 'img/semfoto.jpg', 'F', '(11) 1111-1111', '(11) 1-1111-1111', 'Recepcionista', '', 0, '', '', '', '', '', '2017-08-17', 'S', ''),
('00000000191', 'Administrador do Sistema I', 'e10adc3949ba59abbe56e057f20f883e', 'a@a.com', 'img/00000000191_a1.jpg', 'A', '(11) 1111-11111', '(22) 2-2222-2222', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-15', 'S', ''),
('00000000272', 'Paciente Clinica', '202cb962ac59075b964b07152d234b70', 'p@p.com', 'img/00000000272a4.jpg', 'P', '(77) 7777-7777', '(66) 6-6666-6666', 'Paciente Inglês', 'Praça Professor Mário Bulcão', 12, '121', '1212', '1212', 'SP', '03214070', '2017-06-15', 'S', ''),
('00000000373', 'Funcionário Clinica I', '202cb962ac59075b964b07152d234b70', 'f@f.com', 'img/00000000373_a3.jpg', 'F', '(99) 9999-99999', '(88) 8-8888-8888', 'ffffffffffffffffffffffffffffffffffffffffffffffff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-15', 'S', ''),
('00000000474', 'Médico Clinica I', '202cb962ac59075b964b07152d234b70', 'm@m.com', 'img/00000000474_a5.jpg', 'M', '(55) 5555-55555', '(66) 6-6666-6666', 'mmmmmmmmmmmmmmmmmmmmmmm', '...', 1, '1', '...1111111111111', '...111111111111', 'SP', '48905660', '2017-06-15', 'S', 'Obstetrícia'),
('02371917000128', 'Teste de usuário', 'e10adc3949ba59abbe56e057f20f883e', 'thiagobritocrepaldi@gmail.com', 'img/semfoto.jpg', 'M', '(67) 9999-2222', '(67) 9-9991-111', '', 'Avenida Afonso Pena', 2040, '', 'Centro', 'Campo Grande', 'MS', '79002934', '2017-09-08', NULL, ''),
('27145315807', 'Paciente Clinica II', '7f100b7b36092fb9b06dfb4fac360931', 'p2@p2.com', 'img/27145315807_a6.jpg', 'P', '(88) 8888-8888', '(99) 9-9999-9999', 'Paciente II', '', 0, '', '', '', '', '', '2017-06-17', 'S', ''),
('44171378540', 'Paciente 44171378540', '15d4e891d784977cacbfcbb00c48f133', 'p44@p.com', 'img/semfoto.jpg', 'P', '(11) 1111-1111', '(22) 2-2222-2222', 'ASASASASA', 'Rua São Francisco', 121, '1212', 'São Geraldo', 'Juazeiro', 'BA', '48905660', '2017-09-08', 'S', ''),
('49256742934', 'HEITOR BRASIL', 'e10adc3949ba59abbe56e057f20f883e', 'heitorbrasil@hotmail.com', 'img/semfoto.jpg', 'P', '(41) 3423-6601', '', 'Cadastro efetuado pelo próprio paciente.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-31', 'S', ''),
('78677786325', 'Paciente Clinica III', '202cb962ac59075b964b07152d234b70', 'p3@p3.com', 'img/semfoto.jpg', 'P', '(11) 2367-3107', '', 'Cadastro efetuado pelo próprio paciente', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-20', 'S', '');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`cdusua`),
  ADD KEY `usuarios1` (`deusua`),
  ADD KEY `usuarios2` (`demail`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
